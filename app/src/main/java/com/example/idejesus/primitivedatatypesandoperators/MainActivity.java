package com.example.idejesus.primitivedatatypesandoperators;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //// TODO: 08/06/2017 provide arguments to the functions inside Log.d statements
        Log.d(TAG, "onCreate: " + printName());
        Log.d(TAG, "onCreate: " + add());
        Log.d(TAG, "onCreate: " + concatenateCharacters());
        Log.d(TAG, "onCreate: " + multiply());
        Log.d(TAG, "onCreate: " + isTrue());


    }

    private String printName(String firstName, String lastName){
        // TODO: 08/06/2017 return the full name of the person
        return null;
    }


    private int add(int a, int b){
        // TODO: 08/06/2017 return the sum of two integers
        return 0;
    }


    private long substract(long a, long b){
        // TODO: 08/06/2017 return the difference of two longs
      return 0;
    }

    private char concatenateCharacters(char a, char b){
        // TODO: 08/06/2017 return the concatnation of two characters 
        return a;
    }

    private double multiply(double a, double b){
        // TODO: 08/06/2017 return the product of two doubles
        return 0;
    }

    private boolean isTrue(int a){
        //// TODO: 08/06/2017 return true if a is greater than 10
        return false;
    }
    
}
